from django.db import models

# Create your models here.

class users(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    mobile = models.CharField(max_length=15)
    def __str__(self):
        return (self.name)


class posts(models.Model):
    content=models.CharField(max_length=100)
    postedBy=models.ForeignKey(users,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.postedBy)


class comments(models.Model):
    comment=models.CharField(max_length=100)
    commentOn=models.ForeignKey(posts,on_delete=models.CASCADE)