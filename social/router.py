from .viewsets import UserViewSet,PostsViewSet,CommentsViewSet
from rest_framework import routers
router = routers.DefaultRouter()
router.register('users',UserViewSet)

#  '/api/users' for retrieving and creating users
#  '/api/users/id' for accessing user information and for updating and deleting users.

router.register('posts',PostsViewSet)

# '/api/posts' for retrieving and creating posts
# '/api/posts/id' for accessing user information and for updating and deleting posts.

router.register('comments',CommentsViewSet)

# '/api/comments' for retrieving and creating comments
# '/api/comments/id' for accessing user information and for updating and deleting comments.