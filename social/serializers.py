from rest_framework import serializers
from . models import users,posts,comments
class usersSerializer(serializers.ModelSerializer):
    class Meta:
        model = users
        fields = '__all__'


class postsSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        represent = super().to_representation(instance)
        represent['postedBy'] = usersSerializer(instance.postedBy).data
        return represent
    class Meta:
        model = posts
        fields = ('id','content','postedBy')


class commentsSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        represent = super().to_representation(instance)
        represent['commentOn'] = postsSerializer(instance.commentOn).data
        return represent
    class Meta:

        model = comments
        
        fields = ('id','comment','commentOn')
