from .models import users,posts,comments
from .serializers import usersSerializer,postsSerializer,commentsSerializer
from rest_framework import viewsets
from rest_framework.response import Response

class UserViewSet(viewsets.ModelViewSet):
    queryset=users.objects.all()
    serializer_class = usersSerializer


class PostsViewSet(viewsets.ModelViewSet):
    queryset=posts.objects.all()
    serializer_class = postsSerializer


class CommentsViewSet(viewsets.ModelViewSet):
    queryset=comments.objects.all()
    serializer_class = commentsSerializer