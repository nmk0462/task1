# Generated by Django 3.2 on 2021-05-04 04:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('social', '0011_alter_posts_name1'),
    ]

    operations = [
        migrations.AlterField(
            model_name='posts',
            name='name1',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='social.users'),
        ),
    ]
